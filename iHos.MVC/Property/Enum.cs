﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class IEnum
    {
        public enum JsonMessageType
        {
            primary,
            success,
            info,
            warning,
            danger
        }

        public enum Sorting
        {
            ASC,
            DESC
        }

        public enum Comparison
        {
            ///<summary> A = B</summary>
            Equals,
            ///<summary> A > B</summary>
            GreaterThan,
            ///<summary> A < B </summary>
            LessThan,
            ///<summary> A >= B </summary>
            GreaterThanOrEqualTo,
            ///<summary> A <= B </summary>
            LessThanOrEqualTo,
            ///<summary> A <> B </summary>
            NotEqualTo,
            ///<summary> Custom </summary>
            Custom
        }
    }
}