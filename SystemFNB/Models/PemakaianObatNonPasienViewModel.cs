﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class PemakaianObatNonPasienViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string SectionAsal { get; set; }
        public string SectionTujuan { get; set; }
        public string SectionName { get; set; }
        public bool Batal { get; set; }
        public string Realisasi { get; set; }
        public string SectionAsalID { get; set; }

        [Required]
        public string Keterangan { get; set; }
        public ListDetail<PemakaianObatNonPasienDetailViewModel> Detail_List { get; set; }
    }

    public class PemakaianObatNonPasienDetailViewModel
    {
        public string NoBukti { get; set; }
        public int BarangID { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public Nullable<double> QtyStok { get; set; }
        public double QtyPemakaian { get; set; }
        public decimal Harga { get; set; }
        public string Harga_View { get; set; }
        public string Keterangan { get; set; }
    }

    public class DataObatNonPasienViewModel
    {
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Nama_Kategori { get; set; }
        public string Satuan_Stok { get; set; }
        public Nullable<decimal> Harga_Jual { get; set; }
        public string Harga_Jual_View { get; set; }
        public short Lokasi_ID { get; set; }
        public string SectionID { get; set; }
        public int Barang_ID { get; set; }
        public Nullable<short> KodeSatuan { get; set; }
        public double Qty_Stok { get; set; }
    }
}