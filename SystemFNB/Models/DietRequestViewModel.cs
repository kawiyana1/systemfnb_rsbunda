﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class DietRequestViewModel
    {
        public string No { get; set; }
        public string Noreg { get; set; }
        public string Id_Section { get; set; }
        public string Id_Dokter { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime UntukTanggalDari { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime UntukTanggalSampai { get; set; }
        public string Keterangan { get; set; }
        public string UsesId { get; set; }

        public int Nomor { get; set; }

        public ListDetail<DietRequestDetailViewModel> Detail_List { get; set; }
    }

    public class DietRequestDetailViewModel
    {
        public string No_DietRequest { get; set; }
        public int Kode_Shift { get; set; }
        public string Kode_Makanan { get; set; }
        public string Kode_Produk { get; set; }
        public int Qty { get; set; }
        public string Keterangan { get; set; }
        public Nullable<System.DateTime> Realisasi { get; set; }
        public Nullable<bool> Snack { get; set; }
    }
}