﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class KategoriViewModel
    {
        public int Kode { get; set; }
        public string Nama { get; set; }
        public bool Aktif { get; set; }
    }
}