﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class PenyajianViewModel
    {
        public string No { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public int Kode_Shift { get; set; }
        public string NamaShift { get; set; }
        public string Id_Section { get; set; }
        public string NamaSection { get; set; }
        public string UsesId { get; set; }
        public ListDetail<PenyajianDetailViewModel> Detail_List { get; set; }
    }

    public class PenyajianDetailViewModel
    {
        public string No_Penyajian { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string NoKamar { get; set; }
        public string No_DietRequest { get; set; }
        public string Kode_Produk_Request { get; set; }
        public string Nama_Request { get; set; }
        public string Kode_Makanan { get; set; }
        public string Nama_Makanan { get; set; }
        public string Kode_Produk { get; set; }
        public string NamaProduk { get; set; }
        public int Qty { get; set; }
        public string Keterangan { get; set; }
        public decimal Harga { get; set; }
        public string Harga_View { get; set; }
        public string Penghabisan { get; set; }
        public string BentukMakanan { get; set; }
        public string BentukMakananNama { get; set; }
        public bool Snack { get; set; }
        public string Kode_Snack { get; set; }
        public string Nama_Snack { get; set; }
    }
}