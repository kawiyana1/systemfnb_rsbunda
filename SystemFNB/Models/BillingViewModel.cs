﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class BillingViewModel
    {
        public string No { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.TimeSpan Jam { get; set; }
        public string Jam_View { get; set; }
        public string NoReg { get; set; }
        public string NIK { get; set; }
        public string Id_Dokter { get; set; }
        public string SectionID { get; set; }
        public string TipeTransaksi { get; set; }
        public decimal  Complimant { get; set; }
        public string Complimant_View { get; set; }
        public decimal BebanRumahSakit { get; set; }
        public string BebanRumahSakit_View { get; set; }
        public decimal BayarRoomCharge { get; set; }
        public string BayarRoomCharge_View { get; set; }
        public decimal BayarBonKaryawan { get; set; }
        public string BayarBonKaryawan_View { get; set; }
        public decimal BayarPotongHonor { get; set; }
        public string BayarPotongHonor_View { get; set; }
        public decimal BayarTunai { get; set; }
        public string BayarTunai_View { get; set; }
        public decimal GrandTotal { get; set; }
        public string GrandTotal_View { get; set; }
        public string UsesId { get; set; }
        public bool Audit { get; set; }
        public bool Batal { get; set; }
        public ListDetail<BillingDetailViewModel> Detail_List { get; set; }
    }

    public class BillingDetailViewModel
    {
        public string No_Billing { get; set; }
        public string Kode_Produk { get; set; }
        public string NamaProduk { get; set; }
        public int Nomor { get; set; }
        public decimal Harga { get; set; }
        public string Harga_View { get; set; }
        public int Qty { get; set; }
        public decimal SubTotal { get; set; }
        public string SubTotal_View { get; set; }
        public string Keterangan { get; set; }
    }
}