﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data.Entity.Validation;
using SystemFNB.Entities;

namespace SystemFNB.Models
{
    public static class UserActivity
    {
        public static void InsertUserActivity(UserActivityModel model)
        {
            try
            {
                string fullname;
                using (var s = new UserIdentityEntities())
                {
                    var u = s.AspNetUsers.FirstOrDefault(x => x.Id == model.Id_User);
                    fullname = u == null ? "" : $"{u.FirstName} {u.LastName}";
                }
                using (var s = new UserActivityEntities())
                {
                    var r = s.aFnB.Add(new aFnB()
                    {
                        Id_User = model.Id_User ?? "anonymous",
                        InputDate = model.InputDate,
                        IP = model.IP,
                        OS = model.OS,
                        Browser = model.Browser,
                        Activity = model.Activity,
                        UserFullName = fullname
                    });
                    s.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                string error = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                    foreach (var ve in eve.ValidationErrors)
                        error += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
                }
                throw new Exception("User Activity : " + error);
            }
            catch (SqlException ex) { throw new Exception("User Activity : " + ex.Message); }
            catch (Exception ex) { throw new Exception("User Activity : " + ex.Message); }
            //catch (SqlException ex) { Console.WriteLine(ex.Message); }
            //catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
    }
}