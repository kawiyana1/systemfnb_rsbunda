﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class AmprahStokViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string SectionAsal { get; set; }
        public string SectionTujuan { get; set; }
        public string SectionTujuanName { get; set; }
        public string Keterangan { get; set; }
        public bool Batal { get; set; }
        public string Realisasi { get; set; }
        public string SectionAsalID { get; set; }
        public string SectionAsalName { get; set; }

        public ListDetail<AmprahStokDetailViewModel> Detail_List { get; set; }
    }

    public class AmprahStokDetailViewModel
    {
        public string NoBukti { get; set; }
        public int Barang_ID { get; set; }
        public string Satuan { get; set; }
        public double Qty { get; set; }
        public string StatusBarang { get; set; }
        public bool Realisasi { get; set; }
        public Nullable<double> QtyStok { get; set; }
        public Nullable<double> QtyRealisasiPertama { get; set; }
        public Nullable<double> QtyStokDisini { get; set; }
        public Nullable<double> QtyStokKeluar { get; set; }

        public string Kode_Barang { get; set; }
        public string NamaBarang { get; set; }
        public string SatuanBeli { get; set; }
        public string SatuanStok { get; set; }
        public int Konversi { get; set; }
    }

    public class DataObatUmumViewModel
    {
        public Nullable<int> Barang_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan { get; set; }
        public string SubKategori { get; set; }
        public Nullable<double> Stok { get; set; }
        public string JenisBarang { get; set; }
        public Nullable<int> Kategori_ID { get; set; }
        public string SatuanBeli { get; set; }
        public Nullable<int> Konversi { get; set; }
    }

    public class DataObatStokOpnameViewModel
    {
        //public int Barang_ID { get; set; }
        //public string Kode_Barang { get; set; }
        //public string Nama_Barang { get; set; }
        //public string Nama_Kategori { get; set; }
        //public short Lokasi_ID { get; set; }
        //public string SectionID { get; set; }
        //public string KelompokJenis { get; set; }
        //public string Satuan { get; set; }
        //public Nullable<decimal> Harga { get; set; }
        //public string Harga_View { get; set; }
        //public double QtySystem { get; set; }
        //public Nullable<double> Konversi { get; set; }

        public Nullable<int> Barang_ID { get; set; }
        public string Kode { get; set; }
        public string Desktipsi { get; set; }
        public string Satuan { get; set; }
        public Nullable<double> Konversi { get; set; }
        public Nullable<double> QtySystem { get; set; }
        public Nullable<double> QtyFisik { get; set; }
        public Nullable<double> QtySelisih { get; set; }
        public Nullable<decimal> Harga { get; set; }
        public string Harga_View { get; set; }
        public string Kategori { get; set; }
        public string Keterangan { get; set; }
    }
}