﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class DietViewModel
    {
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public System.DateTime TglInput { get; set; }
        public string TglInput_View { get; set; }
        public System.DateTime JamInput { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoKamar { get; set; }
        public string NamaKelas { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string NamaDOkter { get; set; }
        public string Diagnosa { get; set; }
        public string Keterangan { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }

        public ListDetail<DietDetailViewModel> Detail_List { get; set; }
    }

    public class DietDetailViewModel
    {
        public string NoBukti { get; set; }
        public string Shift { get; set; }
        public string KodeShift{ get; set; }
        public string Nama { get; set; }
        public string Kode_Makanan { get; set; }
        public string IDDiet { get; set; }
        public string NamaDiet { get; set; }
        public Nullable<int> Jumlah { get; set; }
        public string Keterangan { get; set; }
    }

    public class DaftarDietViewModel
    {
        public string IDDiet { get; set; }
        public string NamaDiet { get; set; }
        public string Keterangan { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public Nullable<int> UserIDInput { get; set; }
    }

    public class listDietDetailViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public int KodeShift { get; set; }
        public string Shift { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoKamar { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int Qty { get; set; }
        public string Keterangan { get; set; }
        public Nullable<decimal> Harga { get; set; }
        public Nullable<bool> Realisasi { get; set; }
        public Nullable<bool> Batal { get; set; }
    }
}