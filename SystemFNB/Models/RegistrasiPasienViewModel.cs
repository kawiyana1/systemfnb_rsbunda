﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemFNB.Models
{
    public class RegistrasiPasienViewModel
    {
        public string PenandaKunjunganPertanggal { get; set; }
        public string StatusBayar { get; set; }
        public string NoReg { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }

        public string Emoticon { get; set; }

        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string NoKartu { get; set; }
        public bool RawatInap { get; set; }
        public int NomorNomor { get; set; }
        public string NamaKelas { get; set; }
        public string NamaDOkter { get; set; }
        public string SpesialisName { get; set; }
        public bool SudahPeriksa { get; set; }
        public string NoBill { get; set; }
        public short NoAntri { get; set; }
        public string DokterID { get; set; }
        public bool Batal { get; set; }
        public string SectionAsalID { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public bool MCU { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public Nullable<bool> VIP { get; set; }
        public string VIPKeterangan { get; set; }
        public Nullable<bool> PasienAsuransi { get; set; }
        public string Nama_Asuransi { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public string Pertanggungankeduacompany_name { get; set; }
        public Nullable<bool> PertanggunganKeduaHC { get; set; }
        public Nullable<bool> PertanggunganKeduaIKS { get; set; }
        public Nullable<bool> BPJSExecutive { get; set; }
        public string Alamat { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionAsalName { get; set; }
        public string Kamar { get; set; }
        public string NoBed { get; set; }
        public int Nomor { get; set; }
        public bool Active { get; set; }
        public bool Out { get; set; }
        public string AlasanBatal { get; set; }
        public string KelasID { get; set; }
        public string COmpanyID { get; set; }
        public bool PasienKTP { get; set; }
        public string Keterangan { get; set; }
        public string JabatanDiPerusahaan { get; set; }
        public string JenisPasien { get; set; }
        public string KdKelas { get; set; }
        public string Kode_Customer { get; set; }
        public int NoUrut { get; set; }
        public bool RJ { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<double> Berat { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public bool DariMutasi { get; set; }
        public bool RuangLamaDipakai { get; set; }
        public string KategoriPlafon { get; set; }
        public string NamaKasus { get; set; }
        public Nullable<int> JmlRIThnIni { get; set; }
        public Nullable<int> JmlRIOpnameIni { get; set; }
        public Nullable<bool> Titip { get; set; }
        public string KelasAsalID { get; set; }
        public Nullable<bool> Kerjasama { get; set; }
        public string KamarAsal { get; set; }
        public string NoBedAsal { get; set; }
        public Nullable<double> MarkUp { get; set; }
        public bool TermasukObat { get; set; }
        public string AsuransiID { get; set; }
        public string AssCompanyID_MA { get; set; }
        public Nullable<bool> Paket { get; set; }
        public string PaketJasaID { get; set; }
        public string Nama_Asisting { get; set; }
        public Nullable<decimal> ExcessFee { get; set; }
        public string NoAsuransi { get; set; }
        public Nullable<bool> BayiSAkit { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string KdKelasPertanggungan { get; set; }
        public Nullable<int> KelasPertanggungan_Nomor { get; set; }
        public Nullable<bool> JKN { get; set; }
        public Nullable<bool> BlokJKN { get; set; }
        public Nullable<int> KelasPertanggunganNomor { get; set; }
        public Nullable<bool> KelasPertanggungan_JKN { get; set; }
        public Nullable<bool> NONKELAS { get; set; }
        public Nullable<int> KelasPelayananNomor { get; set; }
        public Nullable<bool> KelasPelayananJKN { get; set; }
        public string PertanggunganKeduaCompanyID { get; set; }
        public string PertanggunganKeduaNoKartu { get; set; }
        public Nullable<int> PertanggunganKeduaCustomerKerjasamaID { get; set; }
        public Nullable<double> Beratbadan { get; set; }
        public Nullable<System.DateTime> TglRes { get; set; }
        public string Diagnosa { get; set; }
        public Nullable<bool> NaikKelas { get; set; }
        public string Phone { get; set; }

        public System.DateTime JamReg { get; set; }
        public bool PasienBaru { get; set; }
        public string NRM_JenisKelamin { get; set; }
        public string NRM_NamaPerusahaan { get; set; }
        public string NRM_NoKartu { get; set; }
        public Nullable<bool> Dimutasikan { get; set; }
        public string DOkterID { get; set; }
        public string Expr1 { get; set; }
        public Nullable<bool> CloseAdmin { get; set; }
        public string UserCloseAdmin { get; set; }
        public Nullable<bool> PasienLoyal { get; set; }
        public Nullable<bool> PasienBlackList { get; set; }
        public string Asuransi { get; set; }
        public string COB_Customer { get; set; }
        public Nullable<bool> DokumenRMBelumLengkap { get; set; }
        public Nullable<int> Umur { get; set; }
        public string DokterRawatID { get; set; }
        public string DokterRawat { get; set; }

        public string EstimasiSisa_View { get; set; }
    }
}