﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SystemFNB.Entities;

namespace SystemFNB.Models
{
    public class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }
        public static List<SelectListItem> SelectListSatuan
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.FNB_mSatuan.Where(x => x.Aktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKategori
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.FNB_mKategori.Where(x => x.Aktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKelompok
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.FNB_mKelompok.Where(x => x.Aktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListShift
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.FNB_mShift.Where(x => x.Aktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static string SelectListSection(int all = 0)
        {
            var result = "";
            if(all == 1) result += $"<option value=\"ALL\">ALL</option>";
            using (var s = new SIMEntities())
            {
                var results = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                foreach (var x in results)
                {
                    result += $"<option value=\"{x.SectionID}\">{x.SectionName}</option>";
                }
            }
            return result;
        }

        public static List<SelectListItem> SelectListProdukDiet
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.FNB_mProduk.Where(x => x.Aktif == true && x.Diet == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListBentukMakanan
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "", Value = ""},
                    new SelectListItem() { Text = "Bubur", Value = "Bubur"},
                    new SelectListItem() { Text = "Bubur Saring", Value = "Bubur Saring"},
                    new SelectListItem() { Text = "Nasi", Value = "Nasi"},
                    new SelectListItem() { Text = "Nasi Tim", Value = "Nasi Tim"}
                };
            }
        }

        public static List<SelectListItem> SelectListPenghabisan(string selectt)
        {
            var m = new List<SelectListItem>() {
                new SelectListItem() { Text = "100%", Value = "100", Selected = (selectt == "100" ? true : false)},
                new SelectListItem() { Text = "75%", Value = "75", Selected = (selectt == "75" ? true : false)},
                new SelectListItem() { Text = "50%", Value = "50", Selected = (selectt == "50" ? true : false)},
                new SelectListItem() { Text = "25%", Value = "25", Selected = (selectt == "25" ? true : false)},
                new SelectListItem() { Text = "0%", Value = "0", Selected = (selectt == "0" ? true : false)}
            };

            return m;
        }

        public static List<SelectListItem> ListSectionStok
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" || x.TipePelayanan == "GUDANG").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionRI
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RI" && x.StatusAktif == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListShift
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.FNB_mShift.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode.ToString()
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListDaftarDiet
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.FNB_mProduk.Where(x => x.Kode_Kategori == 5).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode
                    });
                }
                r.Insert(0, new SelectListItem());
                return r;
            }
        }

        public static List<SelectListItem> ListDaftarMakanan
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.FNB_mProduk.Where(x => x.Kode_Kategori == 1).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Kode
                    });
                    r.Insert(0, new SelectListItem());
                }
                return r;
            }
        }


        public static List<SelectListItem> ListShiftDeskripsi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.FNB_mShift.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.Nama
                    });
                }
                return r;
            }
        }


        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisKelompokObat
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmKelompokJenisObat.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.KelompokJenis,
                            Text = item.KelompokJenis
                        });
                    }
                }
                return r;
            }
        }


    }
}