﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using SystemFNB.Entities;
namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class BillingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoId_FNB_trBilling().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_trBilling> proses = s.FNB_trBilling;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        else
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Today.AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                        else
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Today);
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(FNB_trBilling.No)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(FNB_trBilling.TipeTransaksi)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                    {
                        var f_grand_total = filter[2];
                        proses = proses.Where(x => x.GrandTotal.ToString().Contains(f_grand_total));
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<BillingViewModel>(x));
                    foreach (var x in m)
                    {
                        x.GrandTotal_View = x.GrandTotal.ToMoney();
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.Hours.ToString() + ":" + x.Jam.Minutes.ToString();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var item = new BillingViewModel();
            item.Tanggal = DateTime.Now;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new BillingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<BillingDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0)
                                {
                                    return JsonHelper.JsonMsgInfo("Detail Tidak Boleh Kosong");
                                }
                                if (item.TipeTransaksi == null)
                                {
                                    return JsonHelper.JsonMsgInfo("Tipe Transaksi tidak boleh kosong");
                                }
                                if (item.TipeTransaksi == "BILLING PASIEN")
                                {
                                    if (item.NoReg == null)
                                        return JsonHelper.JsonMsgInfo("Tipe Transaksi Billing Pasien NoReg tidak boleh kosong");
                                }
                                if (item.TipeTransaksi == "DOKTER")
                                {
                                    if (item.NoReg == null)
                                        return JsonHelper.JsonMsgInfo("Tipe Transaksi Dokter Data Dokter tidak boleh kosong");
                                }

                                var total_nilai = item.Complimant_View.ToDecimal() + item.BayarBonKaryawan_View.ToDecimal() + item.BayarPotongHonor_View.ToDecimal()
                                                   + item.BayarRoomCharge_View.ToDecimal() + item.BayarTunai_View.ToDecimal();
                                if(total_nilai != item.GrandTotal_View.ToDecimal())
                                {
                                    return JsonHelper.JsonMsgInfo("Total Nilai dengan nilai pembayaran masih salah");
                                }
                                #endregion

                                var m = IConverter.Cast<FNB_trBilling>(item);
                                m.SectionID = Request.Cookies["SectionIDGizi"].Value;
                                m.Complimant = item.Complimant_View.ToDecimal();
                                m.BebanRumahSakit = item.BebanRumahSakit_View.ToDecimal();
                                m.BayarBonKaryawan = item.BayarBonKaryawan_View.ToDecimal();
                                m.BayarPotongHonor = item.BayarPotongHonor_View.ToDecimal();
                                m.BayarRoomCharge = item.BayarRoomCharge_View.ToDecimal();
                                m.BayarTunai = item.BayarTunai_View.ToDecimal();
                                m.GrandTotal = item.GrandTotal_View.ToDecimal();
                                m.UserId = User.Identity.GetUserId();
                                m.Tanggal = DateTime.Today;
                                m.Jam = DateTime.Now.TimeOfDay;
                                m.Audit = false;
                                m.Batal = false;
                                s.FNB_trBilling.Add(m);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.No_Billing = m.No;
                                    x.Model.Harga = x.Model.Harga_View.ToDecimal();
                                    x.Model.SubTotal = x.Model.SubTotal_View.ToDecimal();
                                }
                                var d = item.Detail_List.ConvertAll(x => IConverter.Cast<FNB_trBillingDetail>(x.Model)).ToArray();
                                foreach (var x in d) { s.FNB_trBillingDetail.Add(x); }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"FNB_trBilling Cerate {m.No}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            BillingViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_trBilling.FirstOrDefault(x => x.No == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<BillingViewModel>(m);
                    item.BayarBonKaryawan_View = m.BayarBonKaryawan.ToMoney();
                    item.BayarPotongHonor_View = m.BayarPotongHonor.ToMoney();
                    item.BayarRoomCharge_View = m.BayarRoomCharge.ToMoney();
                    item.GrandTotal_View = m.GrandTotal.ToMoney();
                    item.BayarTunai_View = m.BayarTunai.ToMoney();
                    var d = s.FNB_trBillingDetail.Where(x => x.Kode_Produk == id).ToList();
                    item.Detail_List = new ListDetail<BillingDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<BillingDetailViewModel>(x);
                        y.Harga_View = y.Harga.ToMoney();
                        y.SubTotal_View = y.SubTotal.ToMoney();
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new BillingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var user = User.Identity.GetUserId();
                                #region Validation
                                var model = s.FNB_trBilling.FirstOrDefault(x => x.No == item.No);
                                if (model == null) throw new Exception("Data Tidak ditemukan");
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<BillingDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);

                                if (item.Detail_List.Count == 0) throw new Exception("Detail Tidak Boleh Kosong");

                                #endregion

                                #region Header
                                model.Id_Dokter = item.Id_Dokter;
                                model.BayarBonKaryawan = item.BayarBonKaryawan_View.ToDecimal();
                                model.BayarPotongHonor = item.BayarPotongHonor_View.ToDecimal();
                                model.BayarRoomCharge = item.BayarRoomCharge_View.ToDecimal();
                                model.GrandTotal = item.GrandTotal_View.ToDecimal();
                                model.BayarTunai = item.BayarTunai_View.ToDecimal();
                                model.NoReg = item.NoReg;
                                model.NIK = item.NIK;
                                #endregion

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.No_Billing = item.No;
                                }
                                var new_list = item.Detail_List;
                                var real_list = s.FNB_trBillingDetail.Where(x => x.No_Billing == item.No).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list)
                                {
                                    var m = new_list.FirstOrDefault(y => y.Model.Nomor == x.Nomor);
                                    if (m == null) s.FNB_trBillingDetail.Remove(x);
                                }

                                foreach (var x in new_list)
                                {
                                    var _m = real_list.FirstOrDefault(y => y.Nomor == x.Model.Nomor);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.FNB_trBillingDetail.Add(new FNB_trBillingDetail()
                                        {
                                            Nomor = x.Model.Nomor,
                                            No_Billing = x.Model.No_Billing,
                                            Kode_Produk = x.Model.Kode_Produk,
                                            Keterangan = x.Model.Keterangan,
                                            Qty = x.Model.Qty,
                                            Harga = x.Model.Harga_View.ToDecimal(),
                                            SubTotal = x.Model.SubTotal_View.ToDecimal()
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Kode_Produk = x.Model.Kode_Produk;
                                        _m.Keterangan = x.Model.Keterangan;
                                        _m.Qty = x.Model.Qty;
                                        _m.Harga = x.Model.Harga_View.ToDecimal();
                                        _m.SubTotal = x.Model.SubTotal_View.ToDecimal();
                                    }
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null)
                                {
                                    if (ex.InnerException.Message == "Conflicting changes detected. This may happen when trying to insert multiple entities with the same key.")
                                    {
                                        throw new Exception("Detail tidak boleh sama");
                                    }
                                }
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D O K T E R
        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mDokter>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== R E G I S T R A S I
        [HttpPost]
        public string ListRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<VW_Registrasi> proses = s.VW_Registrasi.Where(x => x.StatusBayar == "Belum" && x.Batal == false);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(VW_Registrasi.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(VW_Registrasi.NamaPasien)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<VW_Registrasi>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T I P E  T R A N S A S K I
        [HttpPost]
        public string ListTipeTransaksi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmTipeTransaksi_TB> proses = s.SIMmTipeTransaksi_TB;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmTipeTransaksi_TB.TipeTransaksi)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmTipeTransaksi_TB.TipeTransaksi)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmTipeTransaksi_TB>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}