﻿using Newtonsoft.Json;
using SystemFNB.Helper;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using CrystalDecisions.Shared;

namespace SystemFNB.Controllers
{
    public class ReportController : Controller
    {

        private string serverpath = "~/CrystalReport/";
        private string category = "TataBoga";

        #region ==== H REPORT
        public ActionResult Index(string dir = "TataBoga")
        {
            category = dir;
            var hreport = new HReport(Server, serverpath);
            var r = new ReportHelperViewModel()
            {
                Name = category,
                Reports = hreport.List(category),
                UserID = User.Identity.GetUserId(),
                UserName = User.Identity.GetUserName(),
                SectionID = Request.Cookies["SectionIDGizi"].Value,
                SectionName = Request.Cookies["SectionNameGizi"].Value,
            };
            return View(r);
        }

        public ActionResult ExportPDF(string filename, string param)
        {
            try
            {
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["SIMConnectionString"].ConnectionString.Split(';'))
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportPDF(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        public ActionResult print(string nama_file, string nama_sp,string param, int sreport = 0)
        {
            try
            {
                var _param = JsonConvert.DeserializeObject<List<ParameterViewModel>>(param);

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport/Kwitansi"), $"{nama_file}.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand(nama_sp, conn));
                    foreach (var x in _param)
                    {
                        cmd[i].Parameters.Add(new SqlParameter($"@{x.param_name}", x.param_value));
                    }
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"{nama_sp};1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult rekap()
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReport"), $"SIM_Rpt_PenyajianDiet.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_PenyajianDiet", conn));
                    cmd[i].Parameters.Add(new SqlParameter($"@", ""));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_PenyajianDiet;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}