﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using SystemFNB.Entities;
using static SystemFNB.Models.APIMasterServices;
using System.Web.Script.Serialization;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class PenyajianController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post(DateTime tanggal, string nopenyajian, int shift, string section, List<PenyajianDetailViewModel> formData)
        {
            try
            {
                var item = new PenyajianViewModel();
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            item.No = nopenyajian;
                            item.Kode_Shift = shift;
                            item.Id_Section = section;
                            item.Tanggal = DateTime.Now;
                            item.UsesId = User.Identity.GetUserId();
                            var m = IConverter.Cast<FNB_trPenyajian>(item);
                            s.FNB_trPenyajian.Add(m);

                            #region Detail
                            DateTime UntukTanggal = tanggal.Date;
                            var diet = formData.ToArray();
                            if (diet.Count() > 0)
                            {
                                item.Detail_List = new ListDetail<PenyajianDetailViewModel>();
                                foreach (var h in diet)
                                {
                                    
                                    var detaildiet = s.FNB_VWDietRequestDetail.Where(x => x.Noreg == h.NoReg && x.No_DietRequest == h.No_DietRequest && x.Kode_Shift == shift && (x.Realisasi == null || x.Realisasi == false)).ToList();
                                    if (detaildiet.Count() > 0)
                                    {
                                        foreach (var x in detaildiet)
                                        {
                                            item.Detail_List.Add(false, new PenyajianDetailViewModel()
                                            {
                                                NoKamar = x.NoKamar,
                                                Kode_Makanan = x.Kode_Makanan,
                                                Kode_Produk_Request = x.Kode_Produk,
                                                Keterangan = x.Keterangan,
                                                Qty = x.Qty,
                                                NoReg = h.NoReg,
                                                No_DietRequest = h.No_DietRequest
                                            });
                                        }
                                    }
                                    
                                }

                                foreach (var x in formData)
                                {
                                    foreach (var z in item.Detail_List)
                                    {
                                        if(x.No_DietRequest == z.Model.No_DietRequest && x.Kode_Produk_Request == z.Model.Kode_Produk_Request)
                                        {
                                            z.Model.No_Penyajian = nopenyajian;
                                            if (string.IsNullOrEmpty(x.Kode_Produk))
                                            {
                                                return JsonHelper.JsonMsgInfo("Diet tidak boleh kosong");
                                            }
                                            z.Model.NoKamar = x.NoKamar;
                                            z.Model.Kode_Makanan = x.Kode_Makanan;
                                            z.Model.Kode_Produk = x.Kode_Produk;
                                            z.Model.BentukMakanan = x.BentukMakanan;
                                            z.Model.Kode_Snack = x.Kode_Snack;
                                            var produk = s.FNB_mProduk.FirstOrDefault(p => p.Kode == x.Kode_Produk);
                                            if (produk != null)
                                            {
                                                z.Model.Harga = produk.Harga.Value;
                                            }
                                            z.Model.Penghabisan = x.Penghabisan;
                                        }
                                    }
                                }
                            }
                            #endregion

                            var d = item.Detail_List.ConvertAll(z => IConverter.Cast<FNB_trPenyajianDetail>(z.Model)).ToArray();
                            foreach (var y in d) {
                              
                                    s.FNB_trPenyajianDetail.Add(y);

                                    //update trDietRequestDetail
                                    var realisasi = s.FNB_trDietRequestDetail.FirstOrDefault(e => e.No_DietRequest == y.No_DietRequest && e.Kode_Shift == shift);
                                    if (realisasi != null) realisasi.Realisasi = true;
                            }
                            result = new ResultSS(s.SaveChanges());
                            //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            //{
                            //    Activity = $"FNB_trPenyajian Cerate {m.No}"
                            //};
                            //UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                        }
                        catch (SqlException ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);
                        }
                    }
                }
                return JsonHelper.JsonMsgCreate(result, -1);

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            PenyajianViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_trPenyajian.FirstOrDefault(x => x.No == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PenyajianViewModel>(m);
                    var namashift = s.FNB_mShift.FirstOrDefault(e => e.Kode == item.Kode_Shift);
                    if(namashift != null)
                    {
                        item.NamaShift = namashift.Nama;
                    }
                    var d = s.FNB_trPenyajianDetail.Where(x => x.No_Penyajian == id).ToList();
                    item.Detail_List = new ListDetail<PenyajianDetailViewModel>();

                    var registrasimodel = new VW_Registrasi();
                    var sectionmodel = s.SIMmSection.Where(x => x.SectionID == item.Id_Section).FirstOrDefault();
                    if (sectionmodel != null)
                    {
                        item.NamaSection = sectionmodel.SectionName;
                    }
                    foreach (var x in d)
                    {
                        registrasimodel = s.VW_Registrasi.Where(xx => xx.NoReg == x.NoReg).FirstOrDefault();
                        var y = IConverter.Cast<PenyajianDetailViewModel>(x);
                        var kamar = s.FNB_trDietRequestDetail.FirstOrDefault(e => e.No_DietRequest == x.No_DietRequest && e.Kode_Shift == item.Kode_Shift);
                        if(kamar != null)
                        {
                            y.NoKamar = kamar.NoKamar;
                        }

                        var produk = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.Kode_Produk);
                        if (produk != null)
                        {
                            y.NamaProduk = produk.Nama;
                        }

                        var snack = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.Kode_Makanan);
                        if (snack != null)
                        {
                            y.Nama_Makanan = snack.Nama;
                        }

                        var snack2 = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.Kode_Snack);
                        if (snack2 != null)
                        {
                            y.Nama_Snack = snack2.Nama;
                        }

                        if (registrasimodel != null)
                        {
                            y.NRM = registrasimodel.NRM;
                            y.NamaPasien = registrasimodel.NamaPasien;
                            y.JenisKerjasama = registrasimodel.JenisKerjasama;
                        }
                        y.Harga_View = y.Harga.ToMoney();
                        y.BentukMakanan = x.BentukMakanan;
                        var bentukMakanan = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.BentukMakanan);
                        if (bentukMakanan != null)
                        {
                            y.BentukMakananNama = snack.Nama;
                        }
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PenyajianViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var user = User.Identity.GetUserId();

                                #region Validation
                                var model = s.FNB_trPenyajian.FirstOrDefault(x => x.No == item.No);
                                if (model == null) throw new Exception("Data Tidak ditemukan");
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<PenyajianDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);

                                if (item.Detail_List.Count == 0) throw new Exception("Detail Tidak Boleh Kosong");

                                #endregion

                                #region Header
                                #endregion

                                #region Detail
                                var new_list = item.Detail_List;
                                var real_list = s.FNB_trPenyajianDetail.Where(x => x.No_Penyajian == item.No).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list)
                                {
                                    var m = new_list.FirstOrDefault(y => y.Model.Kode_Produk_Request == x.Kode_Produk_Request && y.Model.No_DietRequest == x.No_DietRequest);
                                    if (m == null) s.FNB_trPenyajianDetail.Remove(x);
                                }

                                foreach (var x in new_list)
                                {
                                    var _m = real_list.FirstOrDefault(y => y.Kode_Produk_Request == x.Model.Kode_Produk_Request && y.No_DietRequest == x.Model.No_DietRequest);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        s.FNB_trPenyajianDetail.Add(new FNB_trPenyajianDetail()
                                        {
                                            No_DietRequest = x.Model.No_DietRequest,
                                            Kode_Makanan = x.Model.Kode_Makanan,
                                            Kode_Produk = x.Model.Kode_Produk,
                                            Kode_Produk_Request = x.Model.Kode_Produk_Request,
                                            Harga = x.Model.Harga,
                                            NoReg = x.Model.NoReg,
                                            Keterangan = x.Model.Keterangan,
                                            Penghabisan = x.Model.Penghabisan,
                                            No_Penyajian = x.Model.No_Penyajian,
                                            Qty = x.Model.Qty,
                                            BentukMakanan = x.Model.BentukMakanan,
                                            Snack = x.Model.Snack,
                                            Kode_Snack = x.Model.Kode_Snack

                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Kode_Makanan = x.Model.Kode_Makanan;
                                        _m.Kode_Produk = x.Model.Kode_Produk;
                                        _m.BentukMakanan = x.Model.BentukMakanan;
                                        _m.Penghabisan = x.Model.Penghabisan;
                                        _m.Kode_Snack = x.Model.Kode_Snack;
                                    }
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null)
                                {
                                    if (ex.InnerException.Message == "Conflicting changes detected. This may happen when trying to insert multiple entities with the same key.")
                                    {
                                        throw new Exception("Detail tidak boleh sama");
                                    }
                                }
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_mProduk.FirstOrDefault(x => x.Kode == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.FNB_mProduk.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"FNB_mProduk delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoId_FNB_trPenyajian().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Vw_Penyajian> proses = s.Vw_Penyajian;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        else
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Today.AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                        else
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Today);
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Penyajian.No)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Penyajian.NamaShift)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Penyajian.SectionName)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PenyajianViewModel>(x));
                    var a = new StandardViewModel();
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        if (!string.IsNullOrEmpty(x.Id_Section))
                        {
                            var section = s.SIMmSection.Where(xx => xx.SectionID == x.Id_Section).FirstOrDefault();
                            x.NamaSection = section.SectionName;
                        }
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== S E C T I O N
        [HttpPost]
        public string ListSection(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.TipePelayanan == "RI" || x.TipePelayanan == "RJ");
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmSection>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListShift(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_mShift> proses = s.FNB_mShift;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(FNB_mShift.Kode)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(FNB_mShift.Nama)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ShiftViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== P R O D U K
        [HttpPost]
        public string ListMakanan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_mProduk> proses = s.FNB_mProduk.Where(x => x.Aktif == true && x.Kode_Kategori == 1);
                    var param = filter[0];
                    proses = proses.Where(x => x.Kode.Contains(param) || x.Nama.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ProdukViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListProduk(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_mProduk> proses = s.FNB_mProduk.Where(x => x.Aktif == true && x.Kode_Kategori == 5);
                    var param = filter[0];
                    proses = proses.Where(x => x.Kode.Contains(param) || x.Nama.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ProdukViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListProdukSnack(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_mProduk> proses = s.FNB_mProduk.Where(x => x.Aktif == true && x.Kode_Kategori == 4);
                    var param = filter[0];
                    proses = proses.Where(x => x.Kode.Contains(param) || x.Nama.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ProdukViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T  S E C T I O N
        [HttpPost]
        public string GetSection(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return JsonHelper.JsonMsgError(new Exception("data harus lengkap"));
                var s = new APIMasterServices();
                GetSectionModel model;
                model = s.GetSection(id);
                if (model.Success != true)
                    return JsonHelper.JsonMsgError(new Exception(model.Message));
                var result = new
                {
                    Kode = model.Data.Id,
                    Nama = model.Data.Nama
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== G E T  D A T A  D I E T
        [HttpPost]
        public string GetDataDiet(int shift, string section, DateTime tanggal)
        {
            try
            {
                var item = new PenyajianViewModel();
                using (var s = new SIMEntities())
                {
                    DateTime transpose = tanggal.Date;
                    var m = s.FNB_trDietRequest.Where(x => x.Id_Section == section && x.UntukTanggalDari == transpose).ToArray();
                    //var m = s.FNB_trDietRequest.Where(x => x.Id_Section == section && x.UntukTanggalDari <= DateTime.Today && x.UntukTanggalSampai >= DateTime.Today).ToArray();
                    if (m.Count() > 0)
                    {
                        item = IConverter.Cast<PenyajianViewModel>(m);
                        item.Detail_List = new ListDetail<PenyajianDetailViewModel>();
                        foreach (var h in m)
                        {
                            #region ==== GET
                            var pasien = s.VW_Registrasi.Where(x => x.NoReg == h.Noreg).FirstOrDefault();
                            var jeniskerjasama = pasien.JenisKerjasama;

                            #endregion

                            var d = s.FNB_trDietRequestDetail.Where(x => x.No_DietRequest == h.No && x.Kode_Shift == shift && (x.Realisasi == null || x.Realisasi == false)).ToList();
                            if (d.Count() > 0)
                            {
                                GetRegistrasiModel model;
                                var s1 = new APIMasterServices();
                                foreach (var x in d)    
                                {
                                    var produk = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.Kode_Produk);
                                    var makanan = s.FNB_mProduk.FirstOrDefault(z => z.Kode == x.Kode_Makanan);
                                    var y = new PenyajianDetailViewModel();
                                    y.NoReg = h.Noreg;
                                    y.Qty = x.Qty;
                                    y.Keterangan = x.Keterangan;
                                    y.Kode_Makanan = x.Kode_Makanan;
                                    y.Nama_Makanan = (makanan != null) ? makanan.Nama : "";
                                    y.Kode_Produk_Request = x.Kode_Produk;
                                    y.Nama_Request = produk.Nama;
                                    y.NRM = pasien.NRM;
                                    y.NamaPasien = pasien.NamaPasien + " / " + pasien.Phone;
                                    y.JenisKerjasama = jeniskerjasama;
                                    y.NoKamar = x.NoKamar;
                                    if (produk != null)
                                    {
                                        y.Harga = produk.Harga.Value;
                                        y.Harga_View = produk.Harga.Value.ToMoney();
                                    }
                                    y.Snack = x.Snack == null ? false : x.Snack.Value;
                                    y.No_DietRequest = x.No_DietRequest;
                                    item.Detail_List.Add(false, y);
                                }
                            }
                        }

                    }

                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}