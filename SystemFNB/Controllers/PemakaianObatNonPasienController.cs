﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class PemakaianObatNonPasienController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PemakaianObatNonPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<PemakaianObatNonPasienDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0)
                                {
                                    throw new Exception("Detail Tidak Boleh Kosong");
                                }
                                #endregion

                                var tgl = DateTime.Now;
                                var jam = DateTime.Today;
                                var section = Request.Cookies["SectionIDGizi"].Value;
                                var id = s.AutoNumber_Pelayanan_GD_SIMtrPemakaian().FirstOrDefault();
                                var m = new SIMtrPemakaian()
                                {
                                    NoBukti = id,
                                    Tanggal = tgl,
                                    UserID = 1103,
                                    Jam = jam,
                                    SectionID = section,
                                    Keterangan = item.Keterangan
                                };
                                s.SIMtrPemakaian.Add(m);
                                
                                var simmsection = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    x.Model.NoBukti = m.NoBukti;
                                    var b = s.Pelayanan_GetObatNonPasien.FirstOrDefault(y => y.Barang_ID == x.Model.BarangID && y.SectionID == section);
                                    var n = new SIMtrPemakaianDetail()
                                    {
                                        BarangID = b.Barang_ID,
                                        Harga = b.Harga_Jual ?? 0,
                                        Keterangan = null,
                                        NoBukti = m.NoBukti,
                                        Satuan = b.Satuan_Stok,
                                        QtyStok = b.Qty_Stok, // ????
                                        QtyPemakaian = x.Model.QtyPemakaian
                                    };
                                    s.SIMtrPemakaianDetail.Add(n);
                                    s.IsiKartuGudangFIFO(
                                        simmsection.Lokasi_ID,
                                        b.Barang_ID,
                                        b.Satuan_Stok,
                                        x.Model.QtyPemakaian,
                                        b.Harga_Jual,
                                        m.NoBukti,
                                        564,
                                        0,
                                        tgl.ToString("yyyy-MM-dd"),
                                        tgl.ToString("yyyy-MM-dd"),
                                        0
                                    );
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"PemakaianObatNonPasien Cerate {m.NoBukti}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string id, string alasanbatal)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            var m = s.SIMtrPemakaian.FirstOrDefault(x => x.NoBukti == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");

                            if (m.StatusBatal == true) throw new Exception("Sudah pernah dibatalkan");
                            m.StatusBatal = true;
                            m.JamBatal = DateTime.Now;
                            m.TanggalBatal = DateTime.Today;
                            m.AlasanBatal = alasanbatal;

                            #region Detail
                            var dtl = s.SIMtrPemakaianDetail.Where(x => x.NoBukti == id);
                            var tgl = DateTime.Now;
                            var section = Request.Cookies["SectionIDGizi"].Value;
                            var simmsection = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                            foreach (var x in dtl)
                            {
                                s.IsiKartuGudangFIFO(
                                    simmsection.Lokasi_ID,
                                    x.BarangID,
                                    x.Satuan,
                                    x.QtyPemakaian,
                                    x.Harga,
                                    x.NoBukti + "-R",
                                    562,
                                    1,
                                    tgl.ToString("yyyy-MM-dd"),
                                    tgl.ToString("yyyy-MM-dd"),
                                    0
                                );
                            }
                            #endregion

                            result = new ResultSS(s.SaveChanges());
                            dbContextTransaction.Commit();
                        }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                    }

                    //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    //{
                    //    Activity = $"PemakaianObatNonPasien delete {id}"
                    //};
                    //UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            PemakaianObatNonPasienViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPemakaian.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PemakaianObatNonPasienViewModel>(m);

                    var d = s.SIMtrPemakaianDetail.Where(x => x.NoBukti == id).ToList();
                    item.Detail_List = new ListDetail<PemakaianObatNonPasienDetailViewModel>();
                    foreach (var x in d)
                    {
                        var mbar = s.mBarang.FirstOrDefault(z => z.Barang_ID == x.BarangID);
                        var y = IConverter.Cast<PemakaianObatNonPasienDetailViewModel>(x);
                        y.Harga_View = y.Harga.ToMoney();
                        y.KodeBarang = mbar.Kode_Barang;
                        y.NamaBarang = mbar.Nama_Barang;
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_ListPemakaianObatNonPasien> proses = s.Pelayanan_ListPemakaianObatNonPasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_ListPemakaianObatNonPasien.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_ListPemakaianObatNonPasien.SectionName)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_ListPemakaianObatNonPasien.Keterangan)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Pelayanan_ListPemakaianObatNonPasien.StatusBatal)}=@0", false);
                    proses = proses.Where($"{nameof(Pelayanan_ListPemakaianObatNonPasien.SectionID)}=@0", Request.Cookies["SectionIDGizi"].Value);
                    if (filter[17] != "True" && DateTime.Parse(filter[15]) != null && DateTime.Parse(filter[16]) != null)
                    {
                        proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[15]));
                        proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[16]));
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PemakaianObatNonPasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy") + " " + x.Tanggal.ToString("HH") + ":" + x.Tanggal.ToString("mm");
                        x.SectionTujuan = x.SectionName;
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var section = Request.Cookies["SectionIDGizi"].Value;
                    IQueryable<Pelayanan_GetObatNonPasien> proses = s.Pelayanan_GetObatNonPasien;

                    var simmsection = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Lokasi_ID)}=@0", simmsection.Lokasi_ID);
                    if (IFilter.F_int(filter[0]) != null) proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Barang_ID)}=@0", IFilter.F_int(filter[0]));
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Nama_Barang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Satuan_Stok)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Qty_Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Pelayanan_GetObatNonPasien.Kode_Barang)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DataObatNonPasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}