﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class StokOpnameController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            var section = Request.Cookies["SectionIDGizi"].Value;
            StokOpnameViewModel m;
            using (var s = new SIMEntities())
            {
                var simmsection = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                m = new StokOpnameViewModel()
                {
                    Lokasi = simmsection.SectionName,
                    KelompokJenis = "All"
                };
            }
            if (Request.IsAjaxRequest())
                return PartialView(m);
            else
                return View(m);
        }

        [HttpPost]
        //[ActionName("Create")]
        //[ValidateAntiForgeryToken]
        public string Create_Post(StokOpnameViewModel item)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        s.Database.CommandTimeout = 180;
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                //if (item.Detail_List2 == null)
                                //    item.Detail_List2 = new StokOpnameDetailViewModel();
                                item.Detail_List2.RemoveAll(x => x.Remove);
                                if (item.Detail_List2.Count == 0)
                                {
                                    throw new Exception("Detail Tidak Boleh Kosong");
                                }
                                #endregion

                                var section = Request.Cookies["SectionIDGizi"].Value;
                                var id = s.AutoNumber_Pelayanan_GD_trOpname().FirstOrDefault();
                                var tgl = DateTime.Now;
                                var simmsection = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                                var m = new GD_trOpname()
                                {
                                    No_Bukti = id,
                                    Tgl_Opname = tgl,
                                    Lokasi_ID = simmsection.Lokasi_ID,
                                    User_ID = 1103,
                                    Tgl_Update = tgl,
                                    KelompokJenis = item.KelompokJenis
                                };
                                s.GD_trOpname.Add(m);

                                #region Detail
                                foreach (var x in item.Detail_List2)
                                {
                                    var y = s.Pelayanan_Opname_GetDataBarang(section, item.KelompokJenis).FirstOrDefault(z => z.Barang_ID == x.Barang_ID);
                                    var n = new GD_trOpnameDetail()
                                    {
                                        No_Bukti = m.No_Bukti,
                                        Barang_ID = x.Barang_ID,
                                        Kode_Satuan = y.Satuan,
                                        Stock_Akhir = y.QtySystem,
                                        Qty_Opname = x.QtyFisik,
                                        Harga_Rata = y.Harga,
                                        Keterangan = x.Keterangan,
                                        JenisBarangID = 33
                                    };
                                    s.GD_trOpnameDetail.Add(n);
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"StokOpname Cerate {m.No_Bukti}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            StokOpnameViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.GD_trOpname.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) return HttpNotFound();
                    if (m.Posted) throw new Exception("Data Sudah diposting");
                    item = IConverter.Cast<StokOpnameViewModel>(m);
                    var simmsection = s.SIMmSection.FirstOrDefault(x => x.Lokasi_ID == m.Lokasi_ID);
                    item.Lokasi = simmsection.SectionName;
                    item.Tgl_Opname_View = $"{item.Tgl_Opname.ToString("dd/MM/yyyy")} {item.Tgl_Opname.ToString("HH")}:{item.Tgl_Opname.ToString("mm")}";
                    var d = s.Pelayanan_ViewStokOpname.Where(x => x.No_Bukti == id).ToList();
                    item.Detail_List = new ListDetail<StokOpnameDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<StokOpnameDetailViewModel>(x);
                        y.Harga = (x.Harga_Rata ?? 0).ToMoney();
                        y.Konversi = x.Konversi.ToString();
                        y.Keterangan = x.Keterangan;
                        y.NamaBarang = x.Nama_Barang;
                        y.Satuan = x.Kode_Satuan;
                        y.QtyFisik = (float)(x.Qty_Opname);
                        y.QtySystem = (float)(x.QtySystem ?? 0);
                        y.QtySelisih = y.QtyFisik - y.QtySystem;
                        y.Kategori = x.KelompokJenis;
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            var item = new StokOpnameViewModel();
            TryUpdateModel(item);

            try
            {
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var model = s.GD_trOpname.FirstOrDefault(x => x.No_Bukti == item.No_Bukti);
                                if (model == null) throw new Exception("Data Tidak ditemukan");
                                if (model.Posted) throw new Exception("Data Sudah diposting");
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<StokOpnameDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                if (item.Detail_List.Count == 0)
                                {
                                    throw new Exception("Detail Tidak Boleh Kosong");
                                }
                                #endregion

                                #region Header
                                #endregion

                                #region Detail
                                foreach (var x in item.Detail_List)
                                {
                                    var _m = s.GD_trOpnameDetail.FirstOrDefault(y => y.No_Bukti == item.No_Bukti && y.Barang_ID == x.Model.Barang_ID);
                                    _m.Qty_Opname = x.Model.QtyFisik;
                                    _m.Keterangan = x.Model.Keterangan;
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                //{
                                //    Activity = $"Stok Opname Edit {item.No_Bukti}"
                                //};
                                //UserActivity.InsertUserActivity(userActivity);

                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            StokOpnameViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.GD_trOpname.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<StokOpnameViewModel>(m);
                    var simmsection = s.SIMmSection.FirstOrDefault(x => x.Lokasi_ID == m.Lokasi_ID);
                    item.Lokasi = simmsection.SectionName;
                    item.Tgl_Opname_View = $"{item.Tgl_Opname.ToString("dd/MM/yyyy")} {item.Tgl_Opname.ToString("HH")}:{item.Tgl_Opname.ToString("mm")}";
                    var d = s.Pelayanan_ViewStokOpname.Where(x => x.No_Bukti == id).ToList();
                    item.Detail_List = new ListDetail<StokOpnameDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<StokOpnameDetailViewModel>(x);
                        y.Harga = (x.Harga_Rata ?? 0).ToMoney();
                        y.Konversi = x.Konversi.ToString();
                        y.Keterangan = x.Keterangan;
                        y.NamaBarang = x.Nama_Barang;
                        y.Satuan = x.Kode_Satuan;
                        y.QtyFisik = (float)(x.Qty_Opname);
                        y.QtySystem = (float)(x.QtySystem ?? 0);
                        y.QtySelisih = y.QtyFisik - y.QtySystem;
                        y.Kategori = x.KelompokJenis;
                        item.Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== P R O S E S

        [HttpPost]
        public string Proses(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            var tgl = DateTime.Today.ToString("yyyy-MM-dd");
                            var m = s.GD_trOpname.FirstOrDefault(x => x.No_Bukti == id);
                            if (m == null) throw new Exception("Data Tidak ditemukan");
                            if (m.Posted == false)
                            {
                                m.Posted = true;
                                var dtl = s.GD_trOpnameDetail.Where(x => x.No_Bukti == id);
                                foreach (var x in dtl)
                                {
                                    s.IsiKartuGudangFIFO(
                                        m.Lokasi_ID,
                                        x.Barang_ID,
                                        x.Kode_Satuan,
                                        x.Qty_Opname,
                                        x.Harga_Rata,
                                        m.No_Bukti,
                                        561,
                                        (x.Qty_Opname - x.Stock_Akhir) < 0 ? 0 : 1,
                                        tgl,
                                        tgl,
                                        0
                                    );
                                }
                                s.SaveChanges();
                                dbContextTransaction.Commit();
                            }
                            else
                            {
                                throw new Exception("Sudah pernah di proses");
                            }
                        }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); return JsonHelper.JsonMsgError(ex); }
                    }

                    //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    //{
                    //    Activity = $"StokOpname Proses {id}"
                    //};
                    //UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsg("Berhasil di proses", true);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_ListStokOpname> proses = s.Pelayanan_ListStokOpname;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.No_Bukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.KelompokJenis)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.Status_Batal)}=@0", false);
                    proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.SectionID)}=@0", Request.Cookies["SectionIDGizi"].Value);
                    proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.Tgl_Opname)}>=@0", new DateTime(IFilter.F_int(filter[16]) ?? 0, IFilter.F_int(filter[15]) ?? 0, 1));
                    proses = proses.Where($"{nameof(Pelayanan_ListStokOpname.Tgl_Opname)}<@0", new DateTime(IFilter.F_int(filter[16]) ?? 0, IFilter.F_int(filter[15]) ?? 0, 1).AddMonths(1));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<StokOpnameViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tgl_Opname_View = x.Tgl_Opname.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetail(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var section = Request.Cookies["SectionIDGizi"].Value;
                    IQueryable<Pelayanan_Opname_GetDataBarang_Result> proses = s.Pelayanan_Opname_GetDataBarang(section, filter[11]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_Opname_GetDataBarang_Result.Kode)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_Opname_GetDataBarang_Result.Barang_ID)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DataObatStokOpnameViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Harga_View = (x.Harga ?? 0).ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}