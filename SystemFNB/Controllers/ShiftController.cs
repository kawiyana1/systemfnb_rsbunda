﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using SystemFNB.Entities;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class ShiftController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ShiftViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var has = s.FNB_mShift.Where(x => x.Nama == item.Nama).Count() > 0;
                        if (has) throw new Exception("Nama sudah digunakan");

                        var m = IConverter.Cast<FNB_mShift>(item);
                        s.FNB_mShift.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        //{
                        //    Activity = $"FNB_mShift Create {m.Kode}"
                        //};
                        //UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(int id)
        {
            ShiftViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_mShift.FirstOrDefault(x => x.Kode == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ShiftViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(int id)
        {
            try
            {
                var item = new ShiftViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var model = s.FNB_mShift.FirstOrDefault(x => x.Kode == item.Kode);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.Nama.ToUpper() != item.Nama.ToUpper())
                        {
                            var has = s.FNB_mShift.Where(x => x.Nama == item.Nama).Count() > 0;
                            if (has) throw new Exception("Nama sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        //{
                        //    Activity = $"FNB_mShift Edit {model.Kode}"
                        //};
                        //UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(int id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.FNB_mShift.FirstOrDefault(x => x.Kode == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.FNB_mShift.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"FNB_mShift delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<FNB_mShift> proses = s.FNB_mShift;
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(FNB_mShift.Nama)}.Contains(@0)", filter[1]);
                    if (filter[2] == "1") {
                        proses = proses.Where(x => x.Aktif == true);
                    } else if(filter[2] == "2")
                    {
                        proses = proses.Where(x => x.Aktif == false);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ShiftViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}