﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SystemFNB.Entities;
using SystemFNB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace SystemFNB.Controllers
{
    [Authorize(Roles = "FnB")]
    public class LookUpController : Controller
    {
        #region ===== L I S T  B A H A N

        [HttpPost]
        public string ListBahan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Vw_Bahan> proses = s.Vw_Bahan.Where(x=> x.Aktif == true);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Bahan.Kode)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Bahan.Nama)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Bahan.NamaSatuan)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<BahanViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  P R O D U K

        [HttpPost]
        public string ListProduk(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Vw_Produk> proses = s.Vw_Produk.Where(x => x.Aktif == true);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Produk.Kode)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Produk.Nama)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                    {
                        string harga = filter[2];
                        proses = proses.Where(x => x.Harga.ToString().Contains(harga));
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<ProdukViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}