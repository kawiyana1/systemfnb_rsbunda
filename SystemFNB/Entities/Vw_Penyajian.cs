//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemFNB.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vw_Penyajian
    {
        public string No { get; set; }
        public System.DateTime Tanggal { get; set; }
        public int Kode_Shift { get; set; }
        public string Id_Section { get; set; }
        public string UsesId { get; set; }
        public string NamaShift { get; set; }
        public string SectionName { get; set; }
    }
}
