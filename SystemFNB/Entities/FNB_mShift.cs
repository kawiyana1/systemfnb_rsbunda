//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemFNB.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FNB_mShift
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FNB_mShift()
        {
            this.FNB_trPenyajian = new HashSet<FNB_trPenyajian>();
            this.FNB_trDietRequestDetail = new HashSet<FNB_trDietRequestDetail>();
        }
    
        public int Kode { get; set; }
        public string Nama { get; set; }
        public bool Aktif { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FNB_trPenyajian> FNB_trPenyajian { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FNB_trDietRequestDetail> FNB_trDietRequestDetail { get; set; }
    }
}
