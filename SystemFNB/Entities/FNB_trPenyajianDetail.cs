//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SystemFNB.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FNB_trPenyajianDetail
    {
        public string No_Penyajian { get; set; }
        public string No_DietRequest { get; set; }
        public string Kode_Produk_Request { get; set; }
        public string Kode_Produk { get; set; }
        public int Qty { get; set; }
        public string Keterangan { get; set; }
        public decimal Harga { get; set; }
        public string Penghabisan { get; set; }
        public string NoReg { get; set; }
        public string BentukMakanan { get; set; }
        public Nullable<bool> Snack { get; set; }
        public string NoKamar { get; set; }
        public string Kode_Makanan { get; set; }
        public string Kode_Snack { get; set; }
    }
}
