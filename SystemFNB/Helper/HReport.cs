﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SystemFNB.Helper
{
    public class HReport
    {
        #region ===== P R O P

        public HttpServerUtilityBase _server { get; set; }
        private string _serverpath { get; set; }

        #endregion

        #region ===== P R O C

        private List<HReportModel> GetReports(string path)
        {
            var d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("*.rpt");
            var r = new List<HReportModel>();
            foreach (FileInfo file in Files)
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(path, file.Name));
                var param = new List<HReportParameter>();
                for (var i = 0; i < rd.ParameterFields.Count; i++)
                {
                    param.Add(new HReportParameter()
                    {
                        Name = rd.ParameterFields[i].Name,
                        ParameterType = rd.ParameterFields[i].ParameterValueType
                    });
                }
                r.Add(new HReportModel()
                {
                    FileName = file.Name,
                    Title = rd.SummaryInfo.ReportTitle ?? file.Name,
                    Parameters = param
                });
                rd.Close();
                rd.Dispose();
            }
            return r;
        }

        private TableInfoModel TableInfo(SqlConnection con, string table)
        {
            if (table.Split(';').Count() > 0)
                table = table.Split(';').FirstOrDefault();
            var cmd = new SqlCommand(@"
SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME=@Val;
SELECT ROUTINE_TYPE FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = @Val;
SELECT TOP 10 PARAMETER_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.PARAMETERS WHERE SPECIFIC_NAME = @Val;", con);
            cmd.Parameters.Add(new SqlParameter("@Val", table));
            cmd.CommandType = CommandType.Text;
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return new TableInfoModel("TABLE", table);
            }
            else
            {
                var d = new List<HReportParameter>();
                for (var i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    d.Add(new HReportParameter()
                    {
                        Name = ds.Tables[2].Rows[i].ItemArray[0].ToString(),
                        ParameterType = GetParameterType(ds.Tables[2].Rows[i].ItemArray[1].ToString()),
                    });
                }
                return new TableInfoModel(ds.Tables[1].Rows[0].ItemArray[0].ToString(), table, d);
            }
        }

        private ParameterValueKind GetParameterType(string type)
        {
            //NumberParameter = 0,
            //CurrencyParameter = 1,
            //BooleanParameter = 2,
            //DateParameter = 3,
            //StringParameter = 4,
            //DateTimeParameter = 5,
            //TimeParameter = 6

            type = type.ToUpper();
            if (type == "DATE")
                return ParameterValueKind.DateTimeParameter;
            else if (type == "BIT"
                || type == "TINYINT"
                || type == "SMALLINT"
                || type == "MEDIUMINT"
                || type == "INT"
                || type == "INTEGER"
                || type == "BIGINT"
                || type == "FLOAT"
                || type == "DOUBLE"
                || type == "DOUBLE PRECISION"
                || type == "DECIMAL"
                || type == "DEC"
                || type == "YEAR"
                || type == "NUMBERIC"
                || type == "REAL"
                )
                return ParameterValueKind.NumberParameter;
            else if (type == "BOOL" || type == "BOOLEAN")
                return ParameterValueKind.BooleanParameter;
            else if (type == "DATETIME" || type == "DATETIME2" || type == "SMALLDATETIME")
                return ParameterValueKind.DateTimeParameter;
            else if (type == "TIMESTAMP" || type == "TIME")
                return ParameterValueKind.TimeParameter;
            else if (type == "SMALLMONEY" || type == "MONEY")
                return ParameterValueKind.CurrencyParameter;

            return ParameterValueKind.StringParameter;
        }

        #endregion

        #region ===== P U B L I C

        public HReport(HttpServerUtilityBase server, string serverpath)
        {
            _server = server;
            _serverpath = serverpath;
        }

        public List<HReportModel> List(string category)
        {
            var path = _server.MapPath(_serverpath);
            path = Path.Combine(path, category);
            bool exists = Directory.Exists(path);
            if (!exists) Directory.CreateDirectory(path);
            var r = GetReports(path);
            return r;
        }

        public void Upload(string category, IEnumerable<HttpPostedFileBase> files)
        {
            var path = _server.MapPath(_serverpath);
            path = Path.Combine(path, category);
            bool exists = Directory.Exists(path);
            if (!exists) Directory.CreateDirectory(path);
            foreach (var file in files)
            {
                if (file == null) continue;
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var savepath = Path.Combine(path, fileName);
                    file.SaveAs(savepath);
                }
            }
        }

        public byte[] Download(string category, string filename)
        {
            var path = _server.MapPath(_serverpath);
            path = Path.Combine(path, category);
            path = Path.Combine(path, filename);
            byte[] fileBytes = File.ReadAllBytes(path);
            if (!File.Exists(path)) throw new Exception("File not exist");
            return fileBytes;
        }

        public void Delete(string category, string filename)
        {
            var path = _server.MapPath(_serverpath);
            path = Path.Combine(path, category);
            path = Path.Combine(path, filename);
            if (File.Exists(path))
                File.Delete(path);
        }

        private DataSet ProsesDatabase(string tablename, List<HReportParameter> param, SqlConnection conn) 
        {
            var tblinfo = TableInfo(conn, tablename);
            SqlCommand cmd;
            if (tblinfo.Type == "PROCEDURE")
            {
                cmd = new SqlCommand(tablename, conn);
                foreach (var x in tblinfo.Parameters)
                {
                    var _param = param.FirstOrDefault(y => y.Name == x.Name);
                    var value = _param == null ? "" : _param.Value;
                    if (string.IsNullOrEmpty(value))
                        cmd.Parameters.Add(new SqlParameter(x.Name, null));
                    else if (x.ParameterType == ParameterValueKind.StringParameter || x.ParameterType == ParameterValueKind.NumberParameter || x.ParameterType == ParameterValueKind.CurrencyParameter)
                        cmd.Parameters.Add(new SqlParameter(x.Name, value));
                    else if (x.ParameterType == ParameterValueKind.DateTimeParameter || x.ParameterType == ParameterValueKind.DateParameter)
                        cmd.Parameters.Add(new SqlParameter(x.Name, DateTime.Parse(value)));
                    else if (x.ParameterType == ParameterValueKind.TimeParameter)
                        cmd.Parameters.Add(new SqlParameter(x.Name, TimeSpan.Parse(value)));
                    else if (x.ParameterType == ParameterValueKind.BooleanParameter)
                        cmd.Parameters.Add(new SqlParameter(x.Name, bool.Parse(value)));
                }
                cmd.CommandType = CommandType.StoredProcedure;
            }
            else
            {
                cmd = new SqlCommand("SELECT * FROM " + tblinfo.TableName, conn);
                cmd.CommandType = CommandType.Text;
            }
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public Stream ExportPDF(
            string server,
            string database,
            string userid,
            string password,
            string category,
            string filename,
            List<HReportParameter> param
            )
        {
            try
            {
                var constring = $"Server={server};Database={database};User ID={userid}; Password={password}";
                var path = _server.MapPath(_serverpath + category);

                var rd = new ReportDocument();
                rd.Load(Path.Combine(path, filename));
                //var coninfo = new ConnectionInfo()
                //{
                //    ServerName = server,
                //    DatabaseName = database,
                //    UserID = userid,
                //    Password = password
                //};
                using (var conn = new SqlConnection(constring))
                {
                    for (var i = 0; i < rd.Database.Tables.Count; i++)
                    {
                        //var lg = rd.Database.Tables[i].LogOnInfo;
                        //lg.ConnectionInfo = coninfo;
                        //rd.Database.Tables[i].ApplyLogOnInfo(lg);
                        var ds = ProsesDatabase(rd.Database.Tables[i].Name, param, conn);
                        rd.Database.Tables[i].SetDataSource(ds.Tables[0]);
                    }
                    for (var j = 0; j < rd.Subreports.Count; j++)
                    {
                        for (var i = 0; i < rd.Subreports[j].Database.Tables.Count; i++)
                        {
                            //var lg = rd.Database.Tables[i].LogOnInfo;
                            //lg.ConnectionInfo = coninfo;
                            //rd.Subreports[j].Database.Tables[i].ApplyLogOnInfo(lg);
                            var ds = ProsesDatabase(rd.Subreports[j].Database.Tables[i].Name, param, conn);
                            rd.Subreports[j].Database.Tables[i].SetDataSource(ds.Tables[0]);
                        }
                    }
                    for (var i = 0; i < rd.ParameterFields.Count; i++)
                    {
                        var _m = param.FirstOrDefault(y => y.Name == rd.ParameterFields[i].Name);
                        if (_m == null) continue;
                        var value = _m.Value;
                        if (rd.ParameterFields[i].Name.IndexOf("@Copy") == 0) 
                        {
                            var _copyparam = param.FirstOrDefault(y => y.Name == rd.ParameterFields[i].Name.Replace("@Copy", ""));
                            if (_copyparam != null) value = _copyparam.Value;
                        }
                        var paramtype = rd.ParameterFields[i].ParameterValueType;
                        if (string.IsNullOrEmpty(value))
                            rd.SetParameterValue(_m.Name, null);
                        else if (paramtype == ParameterValueKind.StringParameter || paramtype == ParameterValueKind.NumberParameter || paramtype == ParameterValueKind.CurrencyParameter)
                            rd.SetParameterValue(_m.Name, value);
                        else if (paramtype == ParameterValueKind.DateTimeParameter || paramtype == ParameterValueKind.DateParameter)
                            rd.SetParameterValue(_m.Name, DateTime.Parse(value));
                        else if (paramtype == ParameterValueKind.TimeParameter)
                            rd.SetParameterValue(_m.Name, TimeSpan.Parse(value));
                        else if (paramtype == ParameterValueKind.BooleanParameter)
                            rd.SetParameterValue(_m.Name, bool.Parse(value));
                    }
                }
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                rd.Dispose();
                return stream;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        private class TableInfoModel
        {
            public TableInfoModel(string type, string tblname, List<HReportParameter> param = null) 
            {
                Type = type;
                TableName = tblname;
                Parameters = param;
            }
            public string Type { get; set; }
            public string TableName { get; set; }
            public List<HReportParameter> Parameters { get; set; }
        }
    }

    public class HReportModel
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public List<HReportParameter> Parameters { get; set; }
    }

    public class HReportParameter
    {
        public ParameterValueKind ParameterType { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}